import matplotlib.pyplot as plt
import numpy as np
from scipy import stats

if __name__ == '__main__':
    mean1 = 5
    mean2 = 5
    std1 = 10
    std2 = 10
    sample_size1 = 500
    sample_size2 = 500
    rvs1 = stats.norm.rvs(loc=mean1, scale=std1, size=sample_size1)
    rvs2 = stats.norm.rvs(loc=mean2, scale=std2, size=sample_size2)
    std_pulled = (stats.tvar(rvs1) * (sample_size1 - 1) + stats.tvar(rvs2) * (sample_size2 - 1)) / (
            sample_size1 + sample_size2 - 2)
    statistic_t = (np.mean(rvs1) - np.mean(rvs2)) / np.sqrt(std_pulled / sample_size1 + std_pulled / sample_size2)
    print(statistic_t)
    fig, ax = plt.subplots(1, 2)
    ax[0].hist(rvs1, density=True, histtype='stepfilled', alpha=0.2, color='red')
    ax[1].hist(rvs2, density=True, histtype='stepfilled', alpha=0.2, color='blue')
    plt.show()
    result = stats.ttest_ind(rvs1, rvs2, equal_var=False)
    print(f'Test Statistic: {result.statistic}\np-value:{result.pvalue}')
